# - Find Kakasi
#
#  Kakasi_INCLUDES  - List of Kakasi includes
#  Kakasi_LIBRARIES - List of libraries when using Kakasi.
#  Kakasi_FOUND     - True if Kakasi found.

# Look for the header file.
find_path(Kakasi_INCLUDE NAMES libkakasi.h
        PATHS $ENV{LEVELDB_ROOT}/include /opt/local/include /usr/local/include /usr/include
        DOC "Path in which the file libkakasi.h is located." )

# Look for the library.
find_library(Kakasi_LIBRARY NAMES kakasi
        PATHS /usr/lib $ENV{LEVELDB_ROOT}/lib
        DOC "Path to kakasi library." )

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Kakasi DEFAULT_MSG Kakasi_INCLUDE Kakasi_LIBRARY)

if(KAKASI_FOUND)
    message(STATUS "Found Kakasi (include: ${Kakasi_INCLUDE}, library: ${Kakasi_LIBRARY})")
    set(Kakasi_INCLUDES ${Kakasi_INCLUDE})
    set(Kakasi_LIBRARIES ${Kakasi_LIBRARY})
    mark_as_advanced(Kakasi_INCLUDE Kakasi_LIBRARY)
endif()